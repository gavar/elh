﻿using UnityEditor;
using UnityEngine;

namespace elh.battle.editor
{
	[CustomPropertyDrawer(typeof(Stat))]
	public class StatDrawer : PropertyDrawer
	{
		/// <inheritdoc />
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return 16f;
		}

		/// <inheritdoc />
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			//position = EditorGUI.IndentedRect(position);
			var typeRect = new Rect(position) { width = 150f };
			var valueRect = new Rect(position) { xMin = typeRect.xMax - 30f };

			// TYPE
			property.Next(true);
			property.stringValue = EditorGUI.TextField(typeRect, property.stringValue);
			if (false && EditorGUI.DropdownButton(typeRect, new GUIContent(), FocusType.Passive))
			{
				var path = property.propertyPath;
				var source = property.serializedObject;
				var menu = new GenericMenu();
				GenericMenu.MenuFunction2 func = data =>
				{
					source.FindProperty(path).stringValue = (string)data;
					Debug.Log("set value: " + data + " path: " + path);
					Debug.Log("object: " + source);
				};

				foreach (var value in StatType.TYPES)
					menu.AddItem(new GUIContent(value), false, func, value);

				menu.ShowAsContext();
				source.ApplyModifiedProperties();
			}

			// VALUE
			property.Next(false);
			property.floatValue = EditorGUI.FloatField(valueRect, property.floatValue);
		}
	}
}
