﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections.Generic;
using elh.battle.api;
using elh.game;
using elh.game.entities;
using UnityEngine;

namespace elh.battle
{
	public class BattleController
	{
		protected GameEntityController entityController;
		private readonly List<Stat> _statsBuffer = new List<Stat>();

		public BattleController(GameEntityController entityController)
		{ //
			this.entityController = entityController;
		}

		public IBattleUnit CreateBattleUnit(string actorID)
		{
			var actor = entityController.FindActor(actorID);
			return CreateBattleUnit(actor);
		}

		public IBattleUnit CreateBattleUnit(ActorEntity actor)
		{
			if (actor == null)
				throw new ArgumentNullException("actor");

			var actorStats = actor.stats;

			var unit = new BattleUnit
			{
				Name = actor.ID,
			};

			var stats = unit.Stats = new Stats();
			foreach (var stat in actorStats)
			{
				stats.SetActualOf(stat.type, stat.value);
				stats.SetTotalOf(stat.type, stat.value);
			}

			switch (actor.actorType)
			{
				case ActorType.Interactive:
					stats.SetActualOf(StatType.HEALTH, 1f);
					stats.SetTotalOf(StatType.HEALTH, 1f);
					break;
			}

			return unit;
		}

		public IAttack ResolveAttack(IBattleUnit attacker, IBattleUnit defender)
		{ //
			return VitalyAttack.Shared;
		}

		public void Attack(IBattleUnit attacker, IBattleUnit defender, IList<Stat> results = null)
		{
			var attack = ResolveAttack(attacker, defender);
			Attack(attacker, defender, attack, results);
		}

		public void Attack(IBattleUnit attacker, IBattleUnit defender, IAttack attack, IList<Stat> results = null)
		{
			var defenderStats = defender.Stats;
			try
			{
				if (results == null)
					results = _statsBuffer;

				attack.ResolveAffect(attacker, defender, results);
				for (int i = 0, imax = results.Count; i < imax; i++)
				{
					var stat = results[i];
					defenderStats.ModifyActualOf(stat.type, stat.value);
					var actual = defenderStats.ActualOf(stat.type);
					var total = defenderStats.TotalOf(stat.type);
					Debug.Log(string.Format("defender {0}: {1}/{2}", stat.type, actual, total));
				}

				var health = defender.Stats.ActualOf(StatType.HEALTH);
				if (health <= 0) defender.Dead = true;
			}
			finally
			{
				_statsBuffer.Clear();
			}
		}
	}
}
