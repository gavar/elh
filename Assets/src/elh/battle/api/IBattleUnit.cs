﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.battle.api
{
	public interface IBattleUnit
	{
		string Name { get; set; }
		IStats Stats { get; }
		bool Dead { get; set; }
	}
}
