﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.battle
{
	public interface IStats
	{
		double ActualOf(string key);
		void SetActualOf(string key, double value);
		void ModifyActualOf(string key, double delta);

		void SetTotalOf(string key, double value);
		double TotalOf(string key);

		void ToDefaults();
	}
}
