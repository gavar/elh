﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections.Generic;

namespace elh.battle
{
	public class StatType
	{
		public const string RANGE = "range";
		public const string HEALTH = "health";
		public const string DEFENSE = "defense";
		public const string STRENGTH = "strength";
		public const string ACCURACY = "accuracy";

		public static readonly string[] TYPES =
		{
			RANGE,
			HEALTH,
			DEFENSE,
			STRENGTH,
			ACCURACY,
		};

		public static readonly HashSet<string> Consumables = new HashSet<string>(StringComparer.Ordinal)
		{
			HEALTH,
		};

		public static bool IsConsumable(string statType)
		{ //
			return Consumables.Contains(statType);
		}
	}
}
