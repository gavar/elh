﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections.Generic;
using strange.extensions.signal.impl;

namespace elh.battle
{
	public class Stats : IStats
	{
		protected readonly Dictionary<string, double> actuals;
		protected readonly Dictionary<string, double> totals;

		public Stats()
		{
			var keyEquality = StringComparer.Ordinal;
			actuals = new Dictionary<string, double>(keyEquality);
			totals = new Dictionary<string, double>(keyEquality);
		}

		public readonly Signal onValueChanged = new Signal();

		/// <summary> Get actual value of provided stat type. </summary>
		/// <param name="key"></param>
		public double ActualOf(string key)
		{
			double value;
			if (actuals.TryGetValue(key, out value))
				return value;

			return default(double);
		}

		/// <inheritdoc />
		public void SetActualOf(string key, double value)
		{
			actuals[key] = value;
			onValueChanged.Dispatch();
		}

		/// <inheritdoc />
		public void ModifyActualOf(string key, double delta)
		{
			double value;
			actuals.TryGetValue(key, out value);
			actuals[key] = value + delta;
		}

		/// <inheritdoc />
		public double TotalOf(string key)
		{
			double value;
			if (totals.TryGetValue(key, out value))
				return value;

			return default(double);
		}

		/// <inheritdoc />
		public void SetTotalOf(string key, double value)
		{
			totals[key] = value;
			onValueChanged.Dispatch();
		}

		/// <inheritdoc />
		public void ToDefaults() { }
	}
}
