﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections.Generic;
using elh.battle.api;
using UnityEngine;

namespace elh.battle
{
	public class VitalyAttack : IAttack
	{
		public static readonly VitalyAttack Shared = new VitalyAttack();

		/// <inheritdoc />
		public void ResolveAffect(IBattleUnit attacker, IBattleUnit defender, ICollection<Stat> results)
		{
			var attackerStats = attacker.Stats;
			var defenderStats = defender.Stats;

			var damage = (float)CalculateHealthAffect(attackerStats, defenderStats);
			damage = Mathf.CeilToInt(damage);

			results.Add(new Stat(StatType.HEALTH, -damage));
		}

		private double CalculateHealthAffect(IStats attacker, IStats defender)
		{
			var strength = attacker.ActualOf(StatType.STRENGTH);
			var accuracy = attacker.ActualOf(StatType.ACCURACY);
			var defense = defender.ActualOf(StatType.DEFENSE);

			var accDefDif = defense - accuracy;
			var hitChance = Random.value * 100 + 1.0f;
			hitChance = Mathf.Floor(hitChance);
			var breakout = Mathf.Floor(Random.value * 100 + 1f);

			// check if we actually can deal any damage to defender
			if (accDefDif < 10)
				if (breakout < 6)
					return 0;

			if (accDefDif >= 10 & accDefDif < 21)
			{
				if (breakout < 36)
					return 0;
			}
			else if (accDefDif >= 20 & accDefDif < 31)
			{
				if (breakout < 46)
					return 0;
			}
			else if (accDefDif >= 30 & accDefDif < 41)
			{
				if (breakout < 61)
					return 0;
			}
			else if (accDefDif >= 40 & accDefDif < 51)
			{
				if (breakout < 71)
					return 0;
			}
			else if (accDefDif >= 50 & accDefDif < 61)
			{
				if (breakout < 81)
					return 0;
			}

			// calculate actual hit
			if (hitChance < 6)
				return strength;

			if (hitChance > 5 & hitChance < 16)
				return strength * 0.8f;

			if (hitChance > 15 & hitChance < 36)
				return strength * Random.value;

			return strength * 0.6f;
		}
	}
}
