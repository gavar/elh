﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections.Generic;

namespace elh.battle
{
	[Serializable]
	public struct Stat
	{
		public static int IndexOf(IList<Stat> items, string type)
		{
			if (items == null)
				return -1;

			for (int i = 0, imax = items.Count; i < imax; i++)
			{
				if (items[i].type == type)
					return i;
			}

			return -1;
		}

		public string type;
		public float value;

		/// <inheritdoc />
		public Stat(string type)
		{
			this.type = type;
			value = 0f;
		}

		/// <inheritdoc />
		public Stat(string type, float value)
		{
			this.type = type;
			this.value = value;
		}

		/// <inheritdoc />
		public Stat(string type, double value)
		{
			this.type = type;
			this.value = (float)value;
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return string.Format("{0}: {1}", type, value);
		}
	}
}
