﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace elh.mvcs.main
{
	public class MainContext : MVCSContext
	{
		public MainContext() { }
		public MainContext(MonoBehaviour view) : base(view) { }
		public MainContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags) { }
		public MainContext(MonoBehaviour view, bool autoMapping) : base(view, autoMapping) { }

		/// <inheritdoc />
		public override void Launch()
		{
			base.Launch();
			var view = ((GameObject)contextView).GetComponent<MonoBehaviour>();
			view.StartCoroutine(LaunchRoutine());
		}

		private IEnumerator LaunchRoutine()
		{
			yield return SceneManager.LoadSceneAsync("epic-scene", LoadSceneMode.Additive);
			yield return SceneManager.LoadSceneAsync("ui-scene", LoadSceneMode.Additive);
			SceneManager.SetActiveScene(SceneManager.GetSceneByName("epic-scene"));
		}
	}
}
