﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using strange.extensions.context.impl;

namespace elh.mvcs.common
{
	public class CommonContextView : ContextView
	{
		/// <inheritdoc />
		protected void Awake()
		{
			context = new CommonContext(this);
		}
	}
}
