﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.battle.api;
using elh.game.inventory;
using strange.extensions.signal.impl;
using UnityEngine;

namespace elh.mvcs.common.models
{
	public class Player
	{
		protected object target;

		public IBattleUnit unit;
		public Inventory inventory = new Inventory();

		public readonly Signal onTargetChanged = new Signal();

		public object Target
		{
			get { return target; }
			set
			{
				if (target == value)
					return;

				target = value;
				Debug.Log("Set Target: " + value);
				onTargetChanged.Dispatch();
			}
		}
	}
}
