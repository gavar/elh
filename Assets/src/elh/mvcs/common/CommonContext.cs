﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.game;
using elh.mvcs.common.models;
using elh.mvcs.stage.controllers;
using elh.mvcs.stage.events;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

namespace elh.mvcs.common
{
	public class CommonContext : MVCSContext
	{
		public CommonContext() { }
		public CommonContext(MonoBehaviour view) : base(view) { }
		public CommonContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags) { }
		public CommonContext(MonoBehaviour view, bool autoMapping) : base(view, autoMapping) { }

		/// <inheritdoc />
		protected override void mapBindings()
		{
			base.mapBindings();

			// CROSS CONTEXT EVENTS
			crossContextBridge.Bind(PlayerEvent.ACTOR_CLICK);
			crossContextBridge.Bind(ActorAttackEvent.ACTOR_ATTACKED);

			// MODELS
			injectionBinder.Bind<Player>().ToSingleton().CrossContext();
			injectionBinder.Bind<StageController>().ToSingleton().CrossContext();
			injectionBinder.Bind<GameEntityController>().ToSingleton().CrossContext();
		}
	}
}
