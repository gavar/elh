﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.mvcs.ui.mediators;
using elh.mvcs.ui.views;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

namespace elh.mvcs.ui
{
	public class UIContext : MVCSContext
	{
		public UIContext() { }
		public UIContext(MonoBehaviour view) : base(view) { }
		public UIContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags) { }
		public UIContext(MonoBehaviour view, bool autoMapping) : base(view, autoMapping) { }

		/// <inheritdoc />
		protected override void mapBindings()
		{
			base.mapBindings();

			// MEDIATORS
			mediationBinder.BindView<FighterLinesView>().ToMediator<FighterLinesMediator>();
			mediationBinder.BindView<FighterDamageUI>().ToMediator<FighterDamageUIMediator>();
			mediationBinder.BindView<InventoryUI>().ToMediator<InventoryUIMediator>();
			mediationBinder.BindView<MainDisplayUI>().ToMediator<MainDisplayUIMediator>();
		}
	}
}
