﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.ui;
using strange.extensions.mediation.impl;

namespace elh.mvcs.ui.core
{
	public abstract class ViewController : EventMediator, IRenderer
	{
		protected bool dirty;

		public void SetDirty() { dirty = true; }

		/// <inheritdoc />
		public void Render()
		{
			dirty = false;
			OnRender();
		}

		protected abstract void OnRender();

		/// <inheritdoc />
		protected virtual void Update()
		{
			if (!dirty) return;
			dirty = false;
			OnRender();
		}
	}
}
