﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.mvcs.ui.events
{
	public class WindowEvents
	{
		public static readonly string OPEN_INVENTORY_EVENT = "OPEN_INVENTORY_EVENT";
	}
}
