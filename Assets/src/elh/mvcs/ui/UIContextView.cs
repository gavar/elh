﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using strange.extensions.context.impl;
using UnityEngine;

namespace elh.mvcs.ui
{
	public class UIContextView : ContextView
	{
		public GameObject[] autoDeactivate;

		protected void Awake()
		{
			context = new UIContext(this);
			if (autoDeactivate == null) return;
			foreach (var instance in autoDeactivate)
				if (instance) instance.SetActive(false);
		}
	}
}
