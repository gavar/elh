﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections.Generic;
using elh.mvcs.common.models;
using elh.mvcs.stage.components;
using elh.mvcs.stage.controllers;
using elh.mvcs.stage.events;
using elh.mvcs.ui.core;
using elh.mvcs.ui.views;
using elh.scripts;
using elh.ui.kits.hovers;
using elh.utils;
using UnityEngine;

namespace elh.mvcs.ui.mediators
{
	public class FighterLinesMediator : ViewController
	{
		protected Player player;
		protected StageController stage;
		protected FighterLinesView view;
		protected List<FighterLineView.Data> states;

		[Inject]
		public Player Player { get { return player; } set { player = value; } }

		[Inject]
		public StageController Stage { get { return stage; } set { stage = value; } }

		[Inject]
		public FighterLinesView View { get { return view; } set { view = value; } }

		public FighterLinesMediator()
		{ //
			states = new List<FighterLineView.Data>();
		}

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			stage.actorsModified.AddListener(SetDirty);
			dispatcher.AddListener(PlayerEvent.ACTOR_CLICK, SetDirty);
		}

		/// <inheritdoc />
		protected override void OnRender()
		{
			states.Clear();
			StateByView(stage.PlayerView, states);
			StateByView(player.Target as Component, states);
			view.viewGroup.SetState(states);
		}

		private static void StateByView(Component view, ICollection<FighterLineView.Data> buffer)
		{
			if (view == null) return;
			var battleUnit = view.GetComponent<BattleUnitComponent>();
			var attachPoints = view.GetComponent<AttachPoints>();
			var infoPoint = attachPoints ? attachPoints.GetPointByName("info").point : null;
			buffer.Add(new FighterLineView.Data
			{
				unit = battleUnit != null ? battleUnit.unit : null,
				follow = infoPoint.Transform() ?? view.Transform(),
			});
		}
	}
}
