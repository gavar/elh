﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.mvcs.common.models;
using elh.mvcs.stage.controllers;
using elh.mvcs.ui.core;
using elh.mvcs.ui.events;
using elh.mvcs.ui.views;
using elh.utils;

namespace elh.mvcs.ui.mediators
{
	public class InventoryUIMediator : ViewController
	{
		[Inject]
		public Player Player { get; set; }

		[Inject]
		public InventoryUI View { get; set; }

		[Inject]
		public SpawnController SpawnController { get; set; }

		[Inject]
		public StageController StageController { get; set; }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			dispatcher.AddListener(WindowEvents.OPEN_INVENTORY_EVENT, x => View.SetActive(!View.GetActive()));
			Player.inventory.onChanged.AddListener(x => SetDirty());
			View.SetActive(false);
			View.SetState(Player.inventory);
			View.onUse.AddListener(OnUse);
			View.onChest.AddListener(OnChest);
		}

		protected void OnUse()
		{
			var inventory = Player.inventory;
			var index = View.slotGroup.selection;
			if (index < 0) return;

			var removed = false;
			var item = inventory.items[index];
			switch (item.itemID)
			{
				case "logs":
					removed = inventory.RemoveAt(index);
					var actorView = SpawnController.SpawnActor("bonfire");
					actorView.transform.position = StageController.PlayerView.transform.position;
					break;

				case "bronze-ore":
					removed = inventory.RemoveAt(index);
					inventory.Add("bronze-helmet");
					break;

				case "cooked-meat":
					removed = inventory.RemoveAt(index);
					Player.unit.Stats.ModifyActualOf("health", 3);
					break;
			}

			if (removed)
				View.Deselect();
		}

		private void OnChest()
		{
			var inventory = Player.inventory;
			var index = View.slotGroup.selection;
			if (index < 0) return;
			View.Deselect();
			inventory.RemoveAt(index);
		}

		/// <inheritdoc />
		protected override void OnRender()
		{
			var inventory = Player.inventory;
			View.SetState(inventory);
		}
	}
}
