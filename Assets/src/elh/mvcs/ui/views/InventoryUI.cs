﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.events;
using elh.game;
using elh.game.inventory;
using elh.ui;
using elh.ui.kits.inventory;
using elh.utils;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace elh.mvcs.ui.views
{
	public class InventoryUI : View, IState<Inventory>
	{
		protected Inventory state;
		public InventorySlotGroup slotGroup;
		public GameObject xButton;
		public GameObject useButton;
		public GameObject chestButton;

		protected GameEntityController entityController;
		public readonly Signal onUse = new Signal();
		public readonly Signal onChest = new Signal();

		[Inject]
		public GameEntityController EntityController { get { return entityController; } set { entityController = value; } }

		/// <inheritdoc />
		protected override void Awake()
		{
			this.ComponentRef(ref slotGroup, true);
			base.Awake();
		}

		/// <inheritdoc />
		protected override void Start()
		{
			base.Start();

			xButton.RequireComponent<EventSystemListener>()
				   .onClick.AddListener(x => this.SetActive(false));

			useButton.RequireComponent<EventSystemListener>()
					 .onClick.AddListener(x => onUse.Dispatch());

			chestButton.RequireComponent<EventSystemListener>()
					   .onClick.AddListener(x => onChest.Dispatch());
		}

		/// <inheritdoc />
		protected void OnEnable()
		{
			if (state != null)
				SetState(state);
		}

		/// <inheritdoc />
		public void Deselect()
		{
			slotGroup.selection = -1;
		}

		/// <inheritdoc />
		public void SetState(Inventory next)
		{
			var index = 0;
			var slots = slotGroup.State;
			slots.Clear();
			state = next;

			// item slots
			for (var imax = Mathf.Min(next.slotCapacity, next.items.Count); index < imax; index++)
			{
				var inventoryItem = next.items[index];
				var slot = new InventorySlotView.Data();
				var item = entityController.FindItem(inventoryItem.itemID);
				if (item != null)
				{
					slot.itemID = item.ID;
					slot.count = inventoryItem.amount;
					slot.icon = item.icon;
				}
				slots.Add(slot);
			}

			// empty slots
			for (var imax = next.slotCapacity; index < imax; index++)
			{
				var slot = new InventorySlotView.Data();
				slots.Add(slot);
			}

			slotGroup.SetState(slots);
		}
	}
}
