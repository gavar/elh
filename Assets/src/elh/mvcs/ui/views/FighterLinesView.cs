﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.ui.kits.hovers;
using strange.extensions.mediation.impl;

namespace elh.mvcs.ui.views
{
	public class FighterLinesView : View
	{
		public FighterLineGroup viewGroup;
	}
}
