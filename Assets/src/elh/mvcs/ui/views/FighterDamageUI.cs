﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.utils;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace elh.mvcs.ui.views
{
	public class FighterDamageUI : View
	{
		public FighterDamageRenderer viewRenderer = new FighterDamageRenderer();

		/// <inheritdoc />
		protected override void Awake()
		{
			this.ComponentRef(ref viewRenderer.viewGroup);
		}

		/// <inheritdoc />
		protected void Update()
		{
			viewRenderer.UpdateViewsLife(Time.deltaTime);
			viewRenderer.Update();
		}
	}
}
