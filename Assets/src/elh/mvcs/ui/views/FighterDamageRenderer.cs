// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections.Generic;
using elh.mvcs.stage.views;
using elh.ui.kits.hovers;
using elh.ui.support;
using elh.utils;

namespace elh.mvcs.ui.views
{
	[Serializable]
	public class FighterDamageRenderer : Renderer
	{
		public float viewLifetime = 1f;
		public FighterDamageGroup viewGroup;

		protected readonly List<Data> actors;

		public FighterDamageRenderer()
		{ //
			actors = new List<Data>();
		}

		public void ShowDamage(ActorView actor, float damage)
		{
			Data data;
			var index = IndexOf(actor);
			if (index >= 0) data = actors[index];
			data.actor = actor;
			data.damage = damage;
			data.displayTime = 0f;

			if (index >= 0) actors[index] = data;
			else actors.Add(data);

			SetDirty();
		}

		public void RemoveActor(ActorView actor)
		{
			var index = IndexOf(actor);
			if (index < 0) return;
			actors.RemoveAt(index);
			SetDirty();
		}

		private int IndexOf(ActorView actor)
		{
			for (int i = 0, imax = actors.Count; i < imax; i++)
				if (actors[i].actor == actor)
					return i;

			return -1;
		}

		/// <inheritdoc />
		protected override void OnRender()
		{
			var items = viewGroup.State;
			items.Clear();

			for (int i = 0, imax = actors.Count; i < imax; i++)
			{
				var data = actors[i];
				var actor = data.actor;

				var attachPoints = actor.attachPoints;
				var point = attachPoints.GetPointByName("damage").point;

				var state = new FighterDamageView.Data();
				state.follow = point.Transform() ?? actor.Transform();
				state.damage = data.damage;
				items.Add(state);
			}

			viewGroup.SetState(items);
		}

		public void UpdateViewsLife(float deltaTime)
		{
			for (var i = actors.Count - 1; i >= 0; i--)
			{
				var data = actors[i];
				data.displayTime += deltaTime;
				if (data.displayTime < viewLifetime)
				{
					actors[i] = data;
				}
				else
				{
					actors[i] = default(Data);
				}
			}

			var removed = actors.RemoveAll(x => x.actor == null);
			if (removed > 0) SetDirty();
		}

		public struct Data
		{
			public ActorView actor;
			public float damage;
			public float displayTime;
		}
	}
}
