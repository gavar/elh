// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using strange.extensions.mediation.impl;
using UnityEngine;

namespace elh.mvcs.ui.views
{
	public class MainDisplayUI : View
	{
		public GameObject inventoryButton;
	}
}
