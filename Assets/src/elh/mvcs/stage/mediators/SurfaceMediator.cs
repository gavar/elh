﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.mvcs.stage.events;
using elh.mvcs.stage.views;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace elh.mvcs.stage.mediators
{
	public class SurfaceMediator : EventMediator
	{
		protected SurfaceView view;

		[Inject]
		public SurfaceView View { get { return view; } set { view = value; } }

		/// <inheritdoc />
		protected void Start()
		{
			view.eventListener.onClick.AddListener(x =>
			{
				Debug.Log("Surface click: " + name + " position: " + x.pointerCurrentRaycast.worldPosition);
				dispatcher.Dispatch(SurfaceEvent.SURFACE_CLICK, x);
			});
		}
	}
}
