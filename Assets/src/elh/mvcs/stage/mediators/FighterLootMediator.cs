// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.game;
using elh.mvcs.stage.controllers;
using elh.mvcs.stage.views;
using strange.extensions.mediation.impl;

namespace elh.mvcs.stage.mediators
{
	public class FighterLootMediator : EventMediator
	{
		protected FighterView view;
		protected LootController lootController;
		protected GameEntityController entityController;

		[Inject]
		public FighterView View { get { return view; } set { view = value; } }

		[Inject]
		public GameEntityController EntityController { get { return entityController; } set { entityController = value; } }

		[Inject]
		public LootController LootController { get { return lootController; } set { lootController = value; } }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			view.onDie.AddOnce(OnDie);
		}

		private void OnDie(ActorView actorView)
		{ //
			lootController.SpawnLoot(actorView);
		}
	}
}
