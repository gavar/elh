﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.mvcs.stage.views;
using elh.scripts;
using elh.utils;

namespace elh.mvcs.stage.mediators
{
	public class NPCMediator : ActorMediator
	{
		protected new NPCView view;

		[Inject]
		public NPCView View
		{
			get { return view; }
			set
			{
				view = value;
				base.view = view;
			}
		}

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			view.RequireComponent<TerrainGravity>();
		}
	}
}
