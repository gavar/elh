// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.mvcs.common.models;
using elh.mvcs.stage.views;
using strange.extensions.mediation.impl;
using UnityEngine.EventSystems;

namespace elh.mvcs.stage.mediators
{
	public class LootMediator : Mediator
	{
		protected LootView view;
		protected Player player;

		[Inject]
		public LootView View { get { return view; } set { view = value; } }

		[Inject]
		public Player Player { get { return player; } set { player = value; } }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			view.eventListener.onClick.AddListener(OnLootClick);
		}

		private void OnLootClick(PointerEventData data)
		{
			player.inventory.Add(view.itemID, view.itemCount);
			Destroy(view.gameObject);
		}
	}
}
