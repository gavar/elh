// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.mvcs.stage.controllers;
using elh.mvcs.stage.views;
using strange.extensions.mediation.impl;

namespace elh.mvcs.stage.mediators
{
	public class CameraMediator : EventMediator
	{
		[Inject]
		public CameraView View { get; set; }

		[Inject]
		public StageController Stage { get; set; }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			Stage.CameraView = View;
		}
	}
}
