﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections;
using System.Linq;
using elh.battle;
using elh.game;
using elh.game.entities;
using elh.mvcs.common.models;
using elh.mvcs.stage.components;
using elh.mvcs.stage.controllers;
using elh.mvcs.stage.events;
using elh.mvcs.stage.views;
using elh.scripts;
using elh.utils;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.EventSystems;

namespace elh.mvcs.stage.mediators
{
	public class PlayerMediator : EventMediator
	{
		protected CraftEntity activeCraft;
		protected ActorView interactView;
		protected Coroutine craftRoutine;

		protected Player player;
		protected PlayerView view;
		protected StageController stage;
		protected BattleController battle;
		protected GameEntityController entityController;

		[Inject]
		public PlayerView View { get { return view; } set { view = value; } }

		[Inject]
		public Player Player { get { return player; } set { player = value; } }

		[Inject]
		public StageController Stage { get { return stage; } set { stage = value; } }

		[Inject]
		public BattleController Battle { get { return battle; } set { battle = value; } }

		[Inject]
		public GameEntityController EntityController { get { return entityController; } set { entityController = value; } }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			view.RequireComponent<TerrainGravity>();

			stage.PlayerView = view;
			player.unit = battle.CreateBattleUnit("player");
			player.unit.Name = "RoboChicken";
			view.EquipWeapon("shield", Arm.Left);
			view.EquipWeapon("sword", Arm.Right);
			dispatcher.AddListener(SurfaceEvent.SURFACE_CLICK, OnSurfaceClick);
			dispatcher.AddListener(PlayerEvent.ACTOR_CLICK, OnActorClick);
		}

		protected void Start()
		{
			view.battleUnit.unit = player.unit;
			view.fighterView.autoHitBack = false;
			view.onInteract.AddListener(OnInteract);
			view.onTriggerEnter.AddListener(OnTriggerEnter);
		}

		private void OnSurfaceClick(IEvent e)
		{
			CancelCraft();
			var data = (PointerEventData)e.data;
			var position = data.pointerCurrentRaycast.worldPosition;
			view.fighterView.SetEnemy(null);
			view.ai.SetMoveTarget(position);
		}

		private void OnActorClick(IEvent e)
		{
			var actorView = (ActorView)e.data;
			player.Target = actorView;

			var actorEntity = entityController.FindActor(actorView.actorID);
			Debug.Log("actor click: " + actorEntity.ID, actorView);

			switch (actorEntity.actorType)
			{
				case ActorType.NPC:
					OnNpcClick(actorView, actorEntity);
					break;

				case ActorType.Interactive:
					OnInteractiveClick(actorView, actorEntity);
					break;
			}
		}

		private void CancelCraft()
		{ //
			if (activeCraft == null) return;
			StopCoroutine(craftRoutine);
			StopCraft();
		}

		private void OnTriggerEnter(Collider other)
		{
			var travel = other.GetComponent<TravelPointView>();
			if (travel != null) OnEnterTravelPoint(travel);
		}

		private void OnEnterTravelPoint(TravelPointView travelPoint)
		{
			var destination = travelPoint.destinations[0];
			var toPoint = stage.GetTravelPoint(destination);
			Debug.Log(destination + ": " + toPoint);
			view.ai.SetMoveTarget(null);
			view.transform.position = toPoint.arrival.transform.position;
		}

		private void OnInteractiveClick(ActorView itView, ActorEntity itEntity)
		{
			var craft = itEntity.crafts[0];

			// check previous craft
			if (interactView != itView || activeCraft != craft)
				CancelCraft();

			switch (craft.craftType)
			{
				case CraftType.Mine:
					view.EquipWeapon(null, Arm.Left);
					view.EquipWeapon("pickaxe", Arm.Right);
					break;

				case CraftType.Chop:
					view.EquipWeapon(null, Arm.Left);
					view.EquipWeapon("hatchet", Arm.Right);
					break;

				default:
					view.EquipWeapon(null, Arm.Left);
					view.EquipWeapon(null, Arm.Right);
					break;
			}

			view.fighterView.SetEnemy(null);
			view.ai.SetMoveTarget(itView);
			view.ai.InteractTarget = itView;
			view.ai.SetRange(craft.distance);
		}

		private void OnNpcClick(ActorView actorView, ActorEntity actorEntity)
		{
			CancelCraft();
			var battleUnit = actorView.GetComponent<BattleUnitComponent>();
			var unit = battleUnit.unit;
			var dead = unit != null && unit.Dead;
			view.EquipWeapon("shield", Arm.Left);
			view.EquipWeapon("sword", Arm.Right);
			view.fighterView.SetEnemy(dead ? null : actorView);
		}

		private void OnInteract(ActorView any)
		{
			Debug.Log("interact: " + any);
			var actorEntity = entityController.FindActor(any.actorID);
			switch (actorEntity.actorType)
			{
				case ActorType.Interactive:
					InteractiveAction(any, actorEntity);
					break;
			}
		}

		private void InteractiveAction(ActorView itView, ActorEntity itEntity)
		{
			var craft = itEntity.crafts[0];
			view.ai.SetFaceTarget(itView);
			craftRoutine = StartCoroutine(InteractRoutine(itView, itEntity, craft));
		}

		private void StartCraft(ActorView itView, CraftEntity craft)
		{
			activeCraft = craft;
			interactView = itView;
			view.ai.IsBusy = true;
			view.animator.SetBool(craft.actorStateName, true);
			view.ai.SetFaceTarget(itView);
		}

		private void StopCraft()
		{
			if (activeCraft == null) return;
			view.ai.SetFaceTarget(null);
			view.ai.InteractTarget = null;
			view.animator.SetBool(activeCraft.actorStateName, false);
			view.ai.IsBusy = false;
			activeCraft = null;
			interactView = null;
			craftRoutine = null;
		}

		private IEnumerator InteractRoutine(ActorView itView, ActorEntity itEntity, CraftEntity craft)
		{
			var inventory = player.inventory;
			var interactionView = itView.GetComponent<InteractiveView>();

			if (view.ai.IsBusy)
			{
				Debug.LogError("can't interact: busy!");
				yield break;
			}

			// BUG: consume all items
			var consume = craft.consumes.FirstOrDefault();
			if (consume != null)
			{
				if (inventory.CountOf(consume.itemID) < consume.itemCount)
				{
					Debug.LogError("not enough items: " + consume.itemID);
					yield break;
				}
			}

			StartCraft(itView, craft);
			yield return new WaitForSeconds(craft.duration);

			// BUG: consume all items
			if (consume != null)
				inventory.Remove(consume.itemID, consume.itemCount);

			StopCraft();
			interactionView.onCraftDone.Dispatch(view, craft);
		}
	}
}
