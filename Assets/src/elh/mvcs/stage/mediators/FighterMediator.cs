﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections.Generic;
using elh.battle;
using elh.mvcs.stage.components;
using elh.mvcs.stage.events;
using elh.mvcs.stage.views;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace elh.mvcs.stage.mediators
{
	public class FighterMediator : EventMediator
	{
		protected FighterView view;
		protected BattleController battleController;
		protected IList<Stat> statsBuffer = new List<Stat>();

		[Inject]
		public FighterView View { get { return view; } set { view = value; } }

		[Inject]
		public BattleController BattleController { get { return battleController; } set { battleController = value; } }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			view.onHitBy.AddListener(OnHitBy);
			view.onHitEnemy.AddListener(OnHitEnemy);
		}

		private void OnHitEnemy(object enemy)
		{
			var defenderActor = (ActorView)enemy;
			var defenderUnit = defenderActor.GetComponent<BattleUnitComponent>();
			var defenderFighter = defenderActor.GetComponent<FighterView>();

			var attacker = view.battleUnit.unit;
			var attackerActor = view.GetComponent<ActorView>();

			try
			{
				var defender = defenderUnit.unit;
				battleController.Attack(attacker, defender, statsBuffer);

				if (defender.Dead)
					view.SetEnemy(null);

				defenderFighter.onHitBy.Dispatch(attackerActor);
				dispatcher.Dispatch(ActorAttackEvent.ACTOR_ATTACKED, new ActorAttackEvent
				{
					attacker = attackerActor,
					defender = defenderActor,
					affect = statsBuffer,
				});
			}
			finally
			{
				statsBuffer.Clear();
			}
		}

		private void OnHitBy(object attacker)
		{
			Debug.Log(this + " hit by " + attacker);

			var unit = view.battleUnit.unit;
			if (unit.Dead)
			{
				view.Die();
				return;
			}

			if (view.autoHitBack)
			{
				var actorView = (ActorView)attacker;
				view.SetEnemy(actorView);
			}
		}
	}
}
