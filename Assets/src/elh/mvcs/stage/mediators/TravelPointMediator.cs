// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.mvcs.stage.controllers;
using elh.mvcs.stage.views;
using strange.extensions.mediation.impl;

namespace elh.mvcs.stage.mediators
{
	public class TravelPointMediator : EventMediator
	{
		protected TravelPointView view;
		protected StageController stage;

		[Inject]
		public TravelPointView View { get { return view; } set { view = value; } }

		[Inject]
		public StageController Stage { get { return stage; } set { stage = value; } }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			stage.travelViews.Add(view);
		}

		/// <inheritdoc />
		public override void OnRemove()
		{
			base.OnRemove();
			stage.travelViews.Remove(view);
		}
	}
}
