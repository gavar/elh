// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.mvcs.stage
{
	public static class MemoryVars
	{
		public static readonly string BUSY = "busy";
		public static readonly string DEAD = "dead";
		public static readonly string RANGE = "range";
		public static readonly string BATTLE = "battle";
		public static readonly string CAN_ATTACK = "canAttack";
		public static readonly string FACE_TARGET = "faceTarget";
		public static readonly string MOVE_TARGET = "moveTarget";
		public static readonly string ATTACK_TARGET = "attackTarget";
		public static readonly string CLOSE_DISTANCE = "closeDistance";
		public static readonly string INTERACT_TARGET = "interactTarget";
	}
}
