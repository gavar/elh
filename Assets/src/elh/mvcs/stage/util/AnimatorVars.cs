// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.mvcs.stage
{
	public static class AnimatorVars
	{
		// PARAMETERS
		public static readonly string DEAD = "dead";
		public static readonly string EXIT = "exit";
		public static readonly string ATTACK = "attack";

		// EVENTS
		public const string ATTACK_HIT = "attack-hit";
	}
}
