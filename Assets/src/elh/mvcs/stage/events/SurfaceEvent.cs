﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.mvcs.stage.events
{
	public class SurfaceEvent
	{
		public static readonly string SURFACE_CLICK = "SURFACE_CLICK";
	}
}
