﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections.Generic;
using elh.battle;
using elh.mvcs.stage.views;

namespace elh.mvcs.stage.events
{
	public class ActorAttackEvent
	{
		public static readonly string ACTOR_ATTACKED = "ACTOR_ATTACKED";

		public ActorView attacker;
		public ActorView defender;
		public IList<Stat> affect;
	}
}
