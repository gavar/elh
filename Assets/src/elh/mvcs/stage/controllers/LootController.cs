﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections.Generic;
using elh.game;
using elh.game.entities;
using elh.mvcs.stage.views;
using elh.utils;
using strange.extensions.context.api;
using UnityEngine;

namespace elh.mvcs.stage.controllers
{
	public class LootController
	{
		protected GameObject contextView;
		protected GameEntityController entityController;

		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject ContextView
		{ //
			get { return contextView; }
			set { contextView = value; }
		}

		[Inject]
		public GameEntityController EntityController
		{ //
			get { return entityController; }
			set { entityController = value; }
		}

		public void SpawnLoot(ActorView actorView, float? radius = null)
		{
			var entity = entityController.FindActor(actorView.actorID);
			var loots = entity.loots;
			if (loots == null) return;

			var actorPosition = actorView.transform.position;
			SpawnLoot(entity.loots, actorPosition, radius);
		}

		public void SpawnLoot(IList<RewardEntity> rewards, Vector3 position, float? radius = null)
		{
			foreach (var reward in rewards)
			{
				var direction = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f));
				position += direction * (radius ?? .5f);
				SpawnLoot(reward, position);
			}
		}

		public void SpawnLoot(RewardEntity entity, Vector3 position)
		{
			var item = entityController.FindItem(entity.itemID);
			var instance = Object.Instantiate(item.prefab, contextView.transform);
			instance.name = item.prefab.name;
			instance.transform.position = position;

			var lootView = instance.RequireComponent<LootView>();
			lootView.itemID = item.ID;
			lootView.itemCount = 1;
		}
	}
}
