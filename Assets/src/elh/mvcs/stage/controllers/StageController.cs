﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections.Generic;
using elh.mvcs.stage.views;
using strange.extensions.signal.impl;

namespace elh.mvcs.stage.controllers
{
	public class StageController
	{
		protected CameraView cameraView;
		protected PlayerView playerView;

		public readonly IList<ActorView> actorViews;
		public readonly IList<TravelPointView> travelViews;
		public readonly Signal actorsModified = new Signal();

		/// <inheritdoc />
		public StageController()
		{
			actorViews = new List<ActorView>();
			travelViews = new List<TravelPointView>();
		}

		public CameraView CameraView
		{
			get { return cameraView; }
			set
			{
				cameraView = value;
				UpdateCameraFollow();
			}
		}
		public PlayerView PlayerView
		{
			get { return playerView; }
			set
			{
				playerView = value;
				UpdateCameraFollow();
			}
		}

		public void AddActorView(ActorView actor)
		{
			actorViews.Add(actor);
			actorsModified.Dispatch();
		}
		public void RemoveActorView(ActorView actor)
		{
			actorViews.Remove(actor);
			actorsModified.Dispatch();
		}

		public TravelPointView GetTravelPoint(string destination)
		{
			const StringComparison compareType = StringComparison.OrdinalIgnoreCase;
			foreach (var view in travelViews)
				if (string.Equals(view.departure, destination, compareType))
					return view;

			return null;
		}

		protected void UpdateCameraFollow()
		{
			if (cameraView == null)
				return;

			var transform = playerView != null ? playerView.transform : null;
			cameraView.FollowView = transform;
		}
	}
}
