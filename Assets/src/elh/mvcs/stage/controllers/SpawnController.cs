// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using elh.battle;
using elh.game;
using elh.game.entities;
using elh.mvcs.stage.components;
using elh.mvcs.stage.views;
using elh.utils;
using strange.extensions.context.api;
using UnityEngine;
using Object = UnityEngine.Object;

namespace elh.mvcs.stage.controllers
{
	public class SpawnController
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject ContextView { get; set; }

		[Inject]
		public BattleController BattleController { get; set; }

		[Inject]
		public GameEntityController EntityController { get; set; }

		public ActorView SpawnActor(string actorID, Transform parent = null)
		{
			var entity = EntityController.FindActor(actorID);
			if (entity != null)
				return SpawnActor(entity, parent);

			Debug.LogError("unable to find actor entity: " + actorID);
			return null;
		}

		public ActorView SpawnActor(ActorEntity entity, Transform parent = null)
		{
			ActorView actorView;
			if (parent == null) parent = ContextView.Transform();
			var instance = Object.Instantiate(entity.prefab, parent);
			instance.name = entity.prefab.name;

			switch (entity.actorType)
			{
				case ActorType.NPC:
					actorView = instance.RequireComponent<NPCView>();
					break;

				case ActorType.Interactive:
					actorView = instance.RequireComponent<InteractiveView>();
					break;

				default:
					throw new Exception("Unknown actor type: " + entity.actorType);
			}

			actorView.enabled = true;
			InitializeActor(actorView, entity);
			return actorView;
		}

		public void InitializeActor(ActorView view)
		{
			var entity = EntityController.FindActor(view.actorID);
			if (entity != null) InitializeActor(view, entity);
			else Debug.LogError("unable to find actor entity: " + view.actorID, view);
		}

		public void InitializeActor(ActorView view, ActorEntity entity)
		{
			if (view.initialized) return;
			view.initialized = true;
			view.actorID = entity.ID;

			var battleUnit = view.RequireComponent<BattleUnitComponent>();
			battleUnit.unit = BattleController.CreateBattleUnit(entity);
		}
	}
}
