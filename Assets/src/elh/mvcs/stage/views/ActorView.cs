﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using elh.events;
using elh.mvcs.stage.controllers;
using elh.scripts;
using elh.utils;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;

namespace elh.mvcs.stage.views
{
	public class ActorView : View
	{
		protected StageController stage;

		[NonSerialized]
		public bool initialized;

		public string actorID;
		public AttachPoints attachPoints;
		public EventSystemListener eventListener;
		public readonly Signal<ActorView> onDestroy = new Signal<ActorView>();

		[Inject]
		public StageController Stage { get { return stage; } set { stage = value; } }

		/// <inheritdoc />
		protected override void Awake()
		{
			this.ComponentRef(ref attachPoints);
			this.RequireComponentRef(ref eventListener);
			base.Awake();
		}

		/// <inheritdoc />
		protected override void Start()
		{
			base.Start();
			Stage.AddActorView(this);
		}

		/// <inheritdoc />
		protected override void OnDestroy()
		{
			base.OnDestroy();
			Stage.RemoveActorView(this);
			onDestroy.Dispatch(this);
		}
	}
}
