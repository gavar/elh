﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using strange.extensions.context.api;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace elh.mvcs.stage.views
{
	public class CameraView : View
	{
		protected Camera cacheCamera;
		protected Transform cacheTransform;

		[SerializeField]
		protected Transform followView;
		protected Vector3 followVelocity;

		public float followDistance = 10f;
		public float followTime = 0.5f;

		[Inject(ContextKeys.CONTEXT)]
		public IContext Context { get; set; }

		public Transform FollowView
		{ //
			get { return followView; }
			set { followView = value; }
		}

		protected override void Awake()
		{
			base.Awake();
			cacheTransform = transform;
		}

		protected override void Start()
		{
			base.Start();
			Debug.Log("context: " + Context);
		}

		[PostConstruct]
		public void Setup() { }

		protected void LateUpdate() { UpdateFollow(); }

		protected void UpdateFollow()
		{
			if (followView == null)
			{
				followVelocity = Vector3.zero;
				return;
			}

			var followPosition = followView.position;
			var direction = cacheTransform.forward;
			var ray = new Ray(followPosition, -direction);

			var cameraPosition = cacheTransform.position;
			var toCameraPosition = ray.GetPoint(followDistance);
			cameraPosition = Vector3.SmoothDamp(cameraPosition, toCameraPosition, ref followVelocity, followTime);
			cacheTransform.position = cameraPosition;
		}
	}
}
