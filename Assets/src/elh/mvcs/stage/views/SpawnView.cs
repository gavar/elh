﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace elh.mvcs.stage.views
{
	public class SpawnView : View
	{
		protected float waitTime;

		public readonly Signal<SpawnView> onSpawn = new Signal<SpawnView>();

		public string actorID;
		public int maxCount = 5;
		public float delay = 10f;
		public float radius = 2f;

		[ContextMenu("Spawn")]
		public void Spawn()
		{
			waitTime = 0f;
			onSpawn.Dispatch(this);
		}

		protected override void Start()
		{
			base.Start();
			waitTime = delay;
		}

		protected void Update()
		{
			waitTime += Time.deltaTime;
			if (waitTime < delay) return;
			Spawn();
		}

		protected void OnDrawGizmosSelected()
		{
			var center = transform.position;
			Gizmos.color = new Color(0f, 1f, 1f, 0.5f);
			Gizmos.DrawSphere(center, radius);
		}
	}
}
