﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using strange.extensions.mediation.impl;
using UnityEngine;

namespace elh.mvcs.stage.views
{
	public class TravelPointView : View
	{
		public string departure;
		public string[] destinations;
		public GameObject arrival;
	}
}
