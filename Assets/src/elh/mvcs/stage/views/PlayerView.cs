﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using elh.mvcs.stage.components;
using elh.utils;
using strange.extensions.signal.impl;
using UnityEngine;

namespace elh.mvcs.stage.views
{
	public class PlayerView : ActorView
	{
		public AIComponent ai;
		public Animator animator;
		public FighterView fighterView;
		public BattleUnitComponent battleUnit;
		public WeaponSlot[] weapons;

		public readonly Signal<Collider> onTriggerEnter;
		public readonly Signal<ActorView> onInteract;

		/// <inheritdoc />
		public PlayerView()
		{
			onInteract = new Signal<ActorView>();
			onTriggerEnter = new Signal<Collider>();
		}

		public void EquipWeapon(string weapon, Arm arm)
		{
			for (var i = 0; i < weapons.Length; i++)
			{
				var slot = weapons[i];
				if (slot.arm != arm) continue;
				var match = slot.weapon.name == weapon;
				slot.weapon.SetActive(match);
			}
		}

		public void Interact()
		{
			var actor = ai.InteractTarget;
			ai.InteractTarget = null;
			onInteract.Dispatch(actor);
		}

		protected override void Awake()
		{
			this.ComponentRef(ref animator, true);
			this.RequireComponentRef(ref ai);
			this.RequireComponentRef(ref fighterView);
			this.RequireComponentRef(ref battleUnit);
			base.Awake();
			ai.body = gameObject;
		}

		/// <inheritdoc />
		protected void OnTriggerEnter(Collider other)
		{
			onTriggerEnter.Dispatch(other);
		}

		[Serializable]
		public struct WeaponSlot
		{
			public GameObject weapon;
			public Arm arm;
		}
	}

	public enum Arm
	{
		None,
		Left,
		Right
	}
}
