// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.game.entities;
using elh.mvcs.stage.components;
using elh.utils;
using strange.extensions.signal.impl;

namespace elh.mvcs.stage.views
{
	public class InteractiveView : ActorView
	{
		public BattleUnitComponent battleUnit;
		public readonly Signal<ActorView, CraftEntity> onCraftDone;

		/// <inheritdoc />
		public InteractiveView()
		{
			onCraftDone = new Signal<ActorView, CraftEntity>();
		}

		/// <inheritdoc />
		protected override void Awake()
		{
			this.RequireComponentRef(ref battleUnit);
			base.Awake();
		}
	}
}
