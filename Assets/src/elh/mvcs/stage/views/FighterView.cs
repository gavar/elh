﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections;
using elh.battle;
using elh.mvcs.stage.components;
using elh.scripts;
using elh.utils;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace elh.mvcs.stage.views
{
	public class FighterView : View
	{
		private bool _attacking;
		private bool _attackHitEnemy;

		public AIComponent ai;
		public Animator animator;
		public ActorView actorView;
		public BattleUnitComponent battleUnit;
		public AnimatorEventListener animatorEvents;

		public bool autoHitBack = true;

		public readonly Signal<ActorView> onDie = new Signal<ActorView>();
		public readonly Signal<ActorView> onHitBy = new Signal<ActorView>();
		public readonly Signal<ActorView> onHitEnemy = new Signal<ActorView>();

		protected override void Awake()
		{
			base.Awake();
			this.ComponentRef(ref actorView);
			this.RequireComponentRef(ref ai);
			this.RequireComponentRef(ref battleUnit);

			this.ComponentRef(ref animator, true);
			animator.RequireComponentRef(ref animatorEvents);
			animatorEvents.onAnimationEvent.AddListener(OnAnimatorEvent);
		}

		/// <inheritdoc />
		protected override void Start()
		{
			base.Start();
			ai.CanAttack = true;
		}

		public void SetEnemy(ActorView enemy)
		{
			if (enemy != null)
			{
				Debug.Log("Set Enemy: " + enemy);
				var stats = battleUnit.unit.Stats;
				var range = Mathf.Max((float)stats.ActualOf(StatType.RANGE), 0.5f);

				ai.SetMoveTarget(enemy);
				ai.SetFaceTarget(enemy);
				ai.AttackTarget = enemy;
				ai.SetRange(range);
			}
			else
			{
				ai.AttackTarget = null;
				ai.SetFaceTarget(null);
				ai.SetRange(null);
			}
		}

		public void Hit()
		{
			if (_attacking)
				throw new InvalidOperationException("attacking too fast!");

			_attacking = true;
			_attackHitEnemy = false;
			ai.IsBusy = true;
			ai.CanAttack = false;
			var enemy = ai.AttackTarget;
			StartCoroutine(AttackRoutine(enemy));
		}

		public void Die()
		{
			animator.SetBool(AnimatorVars.DEAD, true);
			ai.IsDead = true;
			ai.SetMoveTarget(null);
			StartCoroutine(DieRoutine());
			onDie.Dispatch(actorView);
		}

		protected IEnumerator AttackRoutine(ActorView enemy)
		{
			try
			{
				animator.SetTrigger(AnimatorVars.ATTACK);

				// wait for hit animation completes
				while (!_attackHitEnemy)
					yield return null;

				onHitEnemy.Dispatch(enemy);
				animator.ResetTrigger(AnimatorVars.ATTACK);
				ai.IsBusy = false;
				yield return new WaitForSeconds(1f);
				ai.CanAttack = true;
			}
			finally
			{
				_attacking = false;
			}
		}

		protected IEnumerator DieRoutine()
		{
			yield return new WaitForSeconds(3f);
			Destroy(gameObject);
		}

		private void OnAnimatorEvent(AnimatorEventListener source)
		{
			switch (source.text)
			{
				case AnimatorVars.ATTACK_HIT:
					_attackHitEnemy = true;
					break;
			}
		}
	}
}
