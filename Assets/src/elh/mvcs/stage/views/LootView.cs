// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.events;
using elh.utils;
using strange.extensions.mediation.impl;

namespace elh.mvcs.stage.views
{
	public class LootView : View
	{
		public string itemID;
		public int itemCount;

		public EventSystemListener eventListener;

		/// <inheritdoc />
		protected override void Awake()
		{
			this.RequireComponentRef(ref eventListener);
			base.Awake();
		}
	}
}
