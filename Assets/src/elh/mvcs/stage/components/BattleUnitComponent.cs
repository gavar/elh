// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.battle.api;
using UnityEngine;

namespace elh.mvcs.stage.components
{
	public class BattleUnitComponent : MonoBehaviour
	{
		public IBattleUnit unit;

		/// <inheritdoc />
		protected void Reset()
		{
			if (unit == null) return;
			var stats = unit.Stats;
			if (stats == null) return;
			stats.ToDefaults();
		}
	}
}
