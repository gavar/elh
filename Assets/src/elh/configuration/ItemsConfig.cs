﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections.Generic;
using elh.game.entities;
using UnityEngine;

namespace elh.configuration
{
	[FilePath("configs/items-config")]
	[CreateAssetMenu(fileName = "items-config", menuName = "ELH/configs/ItemsConfig")]
	public class ItemsConfig : ScriptableObject
	{
		public List<ItemEntity> items = new List<ItemEntity>();
	}
}
