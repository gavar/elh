// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.battle;
using elh.battle.api;
using elh.ui.common;
using elh.utils;
using UnityEngine;
using UnityEngine.UI;

namespace elh.ui.kits.hovers
{
	public class FighterLineView : UState<FighterLineView.Data>
	{
		public Text txtName;
		public FollowView followView;
		public StatLineView statLineView;

		/// <inheritdoc />
		protected override void OnInitialize()
		{
			base.OnInitialize();
			Ref(ref statLineView);
			Require(ref followView);
			statLineView.PatchState(it => it.state.statType = StatType.HEALTH);
		}

		/// <inheritdoc />
		protected override void OnRender()
		{
			var unit = state.unit;
			var unitName = unit != null ? unit.Name : null;
			txtName.SetActive(!string.IsNullOrEmpty(unitName));
			txtName.SetText(unitName);
			followView.Render();
		}

		/// <inheritdoc />
		protected override void OnStateUpdate()
		{
			base.OnStateUpdate();

			var followState = followView.State;
			followState.follow = state.follow;
			followView.State = followState;

			var unit = state.unit;
			var statLineState = statLineView.State;
			statLineState.stats = unit != null ? unit.Stats : null;
			statLineView.State = statLineState;
		}

		public struct Data
		{
			public IBattleUnit unit;
			public Transform follow;
		}
	}
}
