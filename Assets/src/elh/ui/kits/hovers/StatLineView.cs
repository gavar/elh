﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.battle;
using UnityEngine.UI;

namespace elh.ui.kits.hovers
{
	public class StatLineView : UState<StatLineView.Data>
	{
		public Slider slider;

		/// <inheritdoc />
		protected override void OnRender()
		{
			double total = 0;
			double actual = 0;

			var stats = state.stats;
			if (stats != null)
			{
				actual = stats.ActualOf(state.statType);
				total = stats.TotalOf(state.statType);
			}

			slider.minValue = 0f;
			slider.maxValue = (float)total;
			slider.value = (float)actual;
		}

		/// <inheritdoc />
		protected override void Update()
		{
			dirty = true;
			base.Update();
		}

		public struct Data
		{
			public IStats stats;
			public string statType;
		}
	}
}
