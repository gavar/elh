﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.events;
using elh.utils;
using UnityEngine;
using UnityEngine.UI;

namespace elh.ui.kits.inventory
{
	public class InventorySlotView : UState<InventorySlotView.Data>
	{
		public int index;
		public Image imgItem;
		public Image imgSelection;
		public EventSystemListener eventSystemListener;

		/// <inheritdoc />
		protected override void OnInitialize()
		{
			base.OnInitialize();
			Require(ref eventSystemListener);
		}

		/// <inheritdoc />
		protected override void OnRender()
		{
			imgItem.SetActive(state.icon);
			imgItem.overrideSprite = state.icon;
			imgSelection.SetActive(state.selected);
		}

		public struct Data
		{
			public string itemID;
			public int count;
			public Sprite icon;
			public bool selected;
		}
	}
}
