﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;

namespace elh.ui.kits.inventory
{
	public class InventorySlotGroup : UPrefabGroup<InventorySlotView.Data>
	{
		[NonSerialized]
		public int selection;

		/// <inheritdoc />
		protected void OnDisable()
		{
			selection = -1;
			SetDirty();
		}

		/// <inheritdoc />
		protected override void InitializeView(IView<InventorySlotView.Data> view, InventorySlotView.Data state, int index)
		{
			base.InitializeView(view, state, index);
			var it = (InventorySlotView)view;
			it.index = index;
			it.eventSystemListener.onClick.AddListener(x => OnViewClick(it));
		}

		/// <inheritdoc />
		protected override void SetViewState(IView<InventorySlotView.Data> view, InventorySlotView.Data state, int index)
		{
			state.selected = selection == index;
			base.SetViewState(view, state, index);
		}

		protected void OnViewClick(InventorySlotView view)
		{
			var state = view.State;
			if (state.count == 0) return;
			selection = view.index;
			SetDirty();
		}
	}
}
