﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using elh.utils;
using UnityEngine;

namespace elh.ui.common
{
	public class FollowView : UState<FollowView.Data>
	{
		public CanvasNode canvasNode;

		/// <inheritdoc />
		protected override void OnInitialize()
		{
			base.OnInitialize();
			this.RequireComponentRef(ref canvasNode);
		}

		/// <inheritdoc />
		protected override void OnRender()
		{
			var follow = state.follow;
			if (follow == null) return;

			var canvas = canvasNode.rootCanvas;
			var position = follow.position;
			position = Camera.main.WorldToScreenPoint(position);
			switch (canvas.renderMode)
			{
				case RenderMode.ScreenSpaceOverlay:
					break;

				default:
					var camera = canvas.worldCamera;
					if (camera == null) break;
					position = camera.ScreenToWorldPoint(position);
					break;
			}

			position.z = transform.position.z;
			transform.position = position;
		}

		/// <inheritdoc />
		protected override void Update()
		{
			dirty = true;
			base.Update();
		}

		public struct Data
		{
			public Transform follow;
		}
	}
}
