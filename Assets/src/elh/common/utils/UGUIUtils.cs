﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using UnityEngine.UI;

namespace elh.utils
{
	public static class UGUIUtils
	{
		public static void SetText(this Text view, string text)
		{
			if (view)
				view.text = text;
		}
	}
}
