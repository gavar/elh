﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using UnityEngine;

namespace elh.utils
{
	public static class UnityUtils
	{
		public static Transform Transform(this GameObject gameObject)
		{ //
			return gameObject != null ? gameObject.transform : null;
		}

		public static Transform Transform(this Component component)
		{ //
			return component != null ? component.transform : null;
		}

		public static GameObject GameObject(this Component component)
		{ //
			return component != null ? component.gameObject : null;
		}

		public static void SetActive(this Component component, bool activate)
		{
			if (component)
				component.gameObject.SetActive(activate);
		}

		public static bool GetActive(this MonoBehaviour component)
		{ //
			return component && component.isActiveAndEnabled;
		}

		public static void SetEnabled(this MonoBehaviour component, bool enabled)
		{
			if (component)
				component.enabled = enabled;
		}

		public static void ComponentRef<T>(this Component component, ref T value, bool includeChildren = false)
			where T : class
		{
			if (value == null || value as Object == false)
				value = includeChildren
					? component.GetComponentInChildren(typeof(T), true) as T
					: component.GetComponent(typeof(T)) as T;
		}

		public static void ComponentRef<T>(this GameObject gameObject, ref T value, bool includeChildren = false)
			where T : class
		{
			if (gameObject == null)
				return;

			if (value == null || value as Object == false)
				value = includeChildren
					? gameObject.GetComponentInChildren(typeof(T), true) as T
					: gameObject.GetComponent(typeof(T)) as T;
		}

		public static void RequireComponentRef<T>(this Component component, ref T value) where T : Component
		{
			if (value == null)
				value = component.GetComponent(typeof(T)) as T;

			if (value == null)
				value = component.gameObject.AddComponent<T>();
		}
	}
}
