﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using UnityEngine;

namespace elh.utils
{
	public static class ComponentUtils
	{
		public static T RequireComponent<T>(this Component component) where T : Component
		{
			var retVal = component.GetComponent<T>();
			if (retVal) return retVal;
			retVal = component.gameObject.AddComponent<T>();
			return retVal;
		}

		public static T RequireComponent<T>(this GameObject gameObject) where T : Component
		{
			var retVal = gameObject.GetComponent<T>();
			if (retVal) return retVal;
			retVal = gameObject.AddComponent<T>();
			return retVal;
		}
	}
}
