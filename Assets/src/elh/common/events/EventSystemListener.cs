﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.EventSystems;

namespace elh.events
{
	public class EventSystemListener : MonoBehaviour, IPointerClickHandler
	{
		public Signal<PointerEventData> onClick;

		protected void Awake()
		{ //
			onClick = new Signal<PointerEventData>();
		}

		/// <inheritdoc />
		public void OnPointerClick(PointerEventData eventData)
		{
			onClick.Dispatch(eventData);
		}
	}
}
