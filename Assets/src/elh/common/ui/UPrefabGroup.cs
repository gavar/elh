// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace elh.ui
{
	public class UPrefabGroup<T> : UDataGroup<T>
	{
		public GameObject prefab;
		public GameObject viewContainer;
		public List<object> views;

		/// <inheritdoc />
		protected override void OnInitialize()
		{
			if (prefab)
				prefab.SetActive(false);

			base.OnInitialize();

			if (viewContainer == null)
				viewContainer = gameObject;

			if (views == null)
				views = new List<object>();
		}

		/// <inheritdoc />
		public override IList GetViews()
		{
			return views;
		}

		protected GameObject ClonePrefab(int index)
		{
			if (prefab == null)
				throw new InvalidOperationException("prefab is null!");

			var parent = viewContainer.transform;
			var clone = Instantiate(prefab, parent, true);
			clone.name = "item-" + index;
			return clone;
		}

		/// <inheritdoc />
		protected override IView<T> CreateView(T state, int index)
		{
			var clone = ClonePrefab(index);
			var view = clone.GetComponent(typeof(IView<T>));
			if (view == null)
				throw new InvalidOperationException("prefab does not provide view of type: " + typeof(IView<T>));

			return (IView<T>)view;
		}
	}
}
