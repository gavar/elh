﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using UnityEngine;

namespace elh.ui
{
	public class UView : MonoBehaviour, IDisposable, IActivable
	{
		protected bool destroyed;
		protected bool initialized;

		/// <inheritdoc />
		public bool Active
		{ //
			get { return isActiveAndEnabled; }
			set { gameObject.SetActive(value); }
		}

		/// <inheritdoc />
		public void Initialize()
		{
			if (initialized) return;
			OnInitialize();
			initialized = true;
		}

		/// <inheritdoc />
		public void Dispose()
		{
			if (destroyed) return;
			Destroy(this);
		}

		/// <inheritdoc />
		protected void Awake()
		{
			Initialize();
		}

		/// <inheritdoc />
		protected void OnDestroy()
		{
			destroyed = true;
			OnDispose();
		}

		protected virtual void OnInitialize() { }
		protected virtual void OnDispose() { }

		protected void Ref<T>(ref T field) where T : class
		{
			if (field == null)
				field = GetComponent(typeof(T)) as T;
		}

		protected void Require<T>(ref T field) where T : Component
		{
			if (field) return;
			field = gameObject.GetComponent<T>();
			if (field) return;
			field = gameObject.AddComponent<T>();
		}
	}
}
