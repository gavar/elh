﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using UnityEngine;

namespace elh.ui
{
	public abstract class GameObjectView<T> : IView<T>
	{
		public GameObject gameObject;

		protected GameObjectView(GameObject gameObject)
		{ //
			this.gameObject = gameObject;
		}

		/// <inheritdoc />
		public abstract void SetState(T next);

		/// <inheritdoc />
		public bool Active
		{ //
			get { return gameObject.activeInHierarchy; }
			set { gameObject.SetActive(value); }
		}

		/// <inheritdoc />
		public abstract void Render();
	}
}
