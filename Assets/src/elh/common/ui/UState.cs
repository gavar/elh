// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.ui
{
	public abstract class UState<TState> : URenderer, IViewComponent<TState>
	{
		protected TState state;
		protected StateRef<TState> stateRef;

		/// <inheritdoc />
		public TState State
		{
			get { return state; }
			set
			{
				state = value;
				dirty = true;
				OnStateUpdate();
			}
		}

		/// <inheritdoc />
		public void SetState(TState next)
		{
			state = next;
			dirty = true;
			OnStateUpdate();
		}

		/// <inheritdoc />
		public void SetState(ViewUpdater<TState> updater)
		{
			state = updater(state);
			dirty = true;
			OnStateUpdate();
		}

		/// <inheritdoc />
		public void PatchState(ViewPatcher<TState> patcher)
		{
			if (stateRef == null) stateRef = new StateRef<TState>();
			stateRef.state = state;
			patcher(stateRef);
			state = stateRef.state;
			dirty = true;
			OnStateUpdate();
		}

		/// <summary> Occurs when state has been updated. </summary>
		protected virtual void OnStateUpdate() { }
	}
}
