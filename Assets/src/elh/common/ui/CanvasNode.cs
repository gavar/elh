// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using UnityEngine;

namespace elh.ui
{
	public class CanvasNode : MonoBehaviour
	{
		public Canvas rootCanvas;

		/// <inheritdoc />
		protected void OnEnable()
		{
			UpdateCanvas();
		}

		/// <inheritdoc />
		protected void OnCanvasHierarchyChanged()
		{
			UpdateCanvas();
		}

		public void UpdateCanvas()
		{
			rootCanvas = GetComponentInParent<Canvas>();
			while (rootCanvas && !rootCanvas.isRootCanvas)
				rootCanvas = rootCanvas.rootCanvas;
		}
	}
}
