// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using UnityEngine;

namespace elh.configuration
{
	public static class SharedConfig
	{
		public static T GetConfig<T>()
		{ //
			return SharedConfig<T>.shared;
		}

		public static T LoadConfig<T>()
		{
			var path = ConfigUtils.GetConfigPath(typeof(T));
			var resource = Resources.Load(path, typeof(T));
			return (T)(object)resource;
		}
	}

	public static class SharedConfig<T>
	{
		public static T shared;
		static SharedConfig() { shared = SharedConfig.LoadConfig<T>(); }
	}
}
