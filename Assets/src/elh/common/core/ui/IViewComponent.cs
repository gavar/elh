// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.ui
{
	public interface IViewComponent<T> : IView<T>
	{
		void SetState(ViewUpdater<T> updater);
		void PatchState(ViewPatcher<T> patcher);
	}

	public sealed class StateRef<T>
	{
		public T state;
	}

	public delegate T ViewUpdater<T>(T prev);
	public delegate void ViewPatcher<T>(StateRef<T> it);
}
