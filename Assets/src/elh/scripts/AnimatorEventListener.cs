// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using strange.extensions.signal.impl;
using UnityEngine;

namespace elh.scripts
{
	public class AnimatorEventListener : MonoBehaviour
	{
		public int int32;
		public string text;
		public bool debug;

		public readonly Signal<AnimatorEventListener> onAnimationEvent;

		protected AnimatorEventListener()
		{ //
			onAnimationEvent = new Signal<AnimatorEventListener>();
		}

		protected void intEvent(int value)
		{
			try
			{
				int32 = value;
				if (debug) Debug.Log("intEvent: " + value, this);
				onAnimationEvent.Dispatch(this);
			}
			finally
			{
				Clear();
			}
		}

		protected void stringEvent(string value)
		{
			try
			{
				text = value;
				if (debug) Debug.Log("stringEvent: " + value, this);
				onAnimationEvent.Dispatch(this);
			}
			finally
			{
				Clear();
			}
		}

		protected void anim(string value)
		{
			try
			{
				text = value;
				if (debug) Debug.Log("anim: " + value, this);
				onAnimationEvent.Dispatch(this);
			}
			finally
			{
				Clear();
			}
		}

		protected void @event(string value)
		{
			try
			{
				text = value;
				if (debug) Debug.Log("event: " + value, this);
				onAnimationEvent.Dispatch(this);
			}
			finally
			{
				Clear();
			}
		}

		protected void Clear()
		{
			text = null;
			int32 = 0;
		}
	}
}
