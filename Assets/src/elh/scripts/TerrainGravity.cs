﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using UnityEngine;

namespace elh.scripts
{
	public class TerrainGravity : MonoBehaviour
	{
		public Terrain terrain;
		public Transform body;

		private void Awake()
		{
			if (terrain == null)
				terrain = Terrain.activeTerrain;

			if (body == null)
				body = transform;
		}

		/// <inheritdoc />
		protected void Update()
		{
			var position = transform.position;
			var terrainY = terrain.SampleHeight(position);
			if (position.y < terrainY) position.y = terrainY;
			else if (position.y > terrainY) position.y = Mathf.Max(position.y - 9.81f * Time.deltaTime, terrainY);
			else return;
			transform.position = position;
		}
	}
}
