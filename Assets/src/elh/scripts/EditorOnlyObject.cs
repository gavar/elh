// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using UnityEngine;

namespace elh.scripts
{
	public class EditorOnlyObject : MonoBehaviour
	{
		protected void Awake() { gameObject.SetActive(false); }
	}
}
