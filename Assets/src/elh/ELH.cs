// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using UnityEngine;

namespace elh
{
	public class ELH
	{
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		public static void main()
		{
			var resource = Resources.Load("common-context");
			var instance = Object.Instantiate(resource);
			instance.name = resource.name;
			Object.DontDestroyOnLoad(instance);
		}
	}
}
