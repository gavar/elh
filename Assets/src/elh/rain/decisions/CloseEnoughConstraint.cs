// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using RAIN.Action;
using RAIN.Core;
using RAIN.Motion;
using RAIN.Representation;

namespace elh.rain.decisions
{
	[RAINDecision("Close Enough")]
	public class CloseEnoughConstraint : RAINDecision
	{
		private int _lastRunning;
		private readonly MoveLookTarget _moveLookTarget;

		/// <summary> Name of the variable in memory that store wander position. </summary>
		public Expression moveTarget = new Expression();

		public Expression closeEnoughDistance = new Expression();

		public CloseEnoughConstraint()
		{ //
			moveTarget.SetVariable("moveTarget");
			_moveLookTarget = new MoveLookTarget();
		}

		/// <inheritdoc />
		public override void Start(AI ai)
		{
			base.Start(ai);
			_lastRunning = 0;
		}

		/// <inheritdoc />
		public override ActionResult Execute(AI ai)
		{
			if (!moveTarget.IsValid)
				return ActionResult.FAILURE;

			var memory = ai.WorkingMemory;
			var deltaTime = ai.DeltaTime;

			// ignore y
			_moveLookTarget.ObjectTarget = moveTarget.Evaluate<object>(deltaTime, memory);
			var position = _moveLookTarget.Position;
			position.y = ai.Kinematic.Position.y;
			_moveLookTarget.VectorTarget = position;

			_moveLookTarget.CloseEnoughDistance = closeEnoughDistance.Evaluate<float>(deltaTime, memory);

			if (!ai.Navigator.IsAt(_moveLookTarget))
				return ActionResult.FAILURE;

			if (_children.Count < 1)
				return ActionResult.SUCCESS;

			var result = ActionResult.FAILURE;
			for (; _lastRunning < _children.Count; _lastRunning++)
			{
				result = _children[_lastRunning].Run(ai);
				if (result != ActionResult.SUCCESS)
					break;
			}
			return result;
		}
	}
}
