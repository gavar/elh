﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.utils;
using RAIN.Core;
using RAIN.Serialization;
using UnityEngine;

namespace elh.rain.elements
{
	[RAINSerializableClass, RAINElement("Animator Motion")]
	public class AnimatorMotion : CustomAIElement
	{
		protected Animator animator;

		[RAINSerializableField(Visibility = FieldVisibility.Show, ToolTip = "Name of the velocity parameter")]
		public string speedName = "speed";

		/// <inheritdoc />
		public override void AIStart()
		{
			var body = AI.Body;
			body.ComponentRef(ref animator, true);
		}

		/// <inheritdoc />
		public override void Post()
		{
			base.Post();
			var speed = AI.Kinematic.Speed;
			animator.SetFloat(speedName, speed);
		}
	}
}
