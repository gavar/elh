// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

namespace elh.rain.actions
{
	[RAINAction("SendMessage")]
	public class SendMessageAction : RAINAction
	{
		public Expression method = new Expression();
		public Expression param = new Expression();

		/// <inheritdoc />
		public override ActionResult Execute(AI ai)
		{
			var methodName = method.VariableName;
			if (param.IsValid)
			{
				var value = param.Evaluate<object>(ai.DeltaTime, ai.WorkingMemory);
				ai.Body.SendMessage(methodName, value);
				return ActionResult.SUCCESS;
			}

			ai.Body.SendMessage(methodName);
			return ActionResult.SUCCESS;
		}
	}
}
