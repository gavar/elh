// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.game.entities
{
	public enum DeliveryType
	{
		None,
		Loot = 1,
		Spawn = 2,
		Inventory = 3,
	}
}
