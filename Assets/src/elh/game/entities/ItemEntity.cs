﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using UnityEngine;

namespace elh.game.entities
{
	[Serializable]
	public class ItemEntity
	{
		public string ID;
		public string name;
		public Sprite icon;
		public GameObject prefab;
	}
}
