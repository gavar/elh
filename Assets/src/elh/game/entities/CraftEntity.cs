// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;

namespace elh.game.entities
{
	[Serializable]
	public class CraftEntity
	{
		public CraftType craftType;
		public float duration = 15f;
		public float distance = 0.5f;
		public ConsumeEntry[] consumes;
		public RewardEntity[] rewards;
		public string actorStateName;
	}
}
