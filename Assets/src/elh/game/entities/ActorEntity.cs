﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using elh.battle;
using UnityEngine;

namespace elh.game.entities
{
	[Serializable]
	public class ActorEntity
	{
		public string ID;
		public ActorType actorType;
		public GameObject prefab;

		public Stat[] stats;
		public RewardEntity[] loots;
		public CraftEntity[] crafts;

		public ActorEntity()
		{
			stats = new[]
			{
				new Stat
				{
					type = StatType.HEALTH,
					value = 100f,
				}
			};
		}
	}
}
