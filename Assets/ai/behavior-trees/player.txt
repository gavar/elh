<behaviortree debugbreak="False" name="player" repeatuntil="" version="1.1">
	<selector debugbreak="False" name="root" repeatuntil="" usepriorities="False">
		<constraint constraint="busy" debugbreak="False" name="busy?" priority="" repeatuntil="">
			<move closeenoughangle="" closeenoughdistance="" debugbreak="False" facetarget="faceTarget" movespeed="" movetarget="" name="face target" repeatuntil="" turnspeed=""/>
		</constraint>
		<parallel debugbreak="False" fail="any" name="behave" priority="" repeatuntil="" succeed="all" tiebreaker="fail">
			<selector debugbreak="False" name="action" repeatuntil="" usepriorities="False">
				<constraint constraint="attackTarget != null" debugbreak="False" name="attack?" priority="" repeatuntil="">
					<decision classname="elh.rain.decisions.CloseEnoughConstraint" debugbreak="False" name="close enough?" namespace="" parameters="bW92ZVRhcmdldABjbG9zZUVub3VnaERpc3RhbmNl" parametervalues="bW92ZVRhcmdldAByYW5nZQ==" repeatuntil="">
						<expression debugbreak="False" expression="canAttack" name="can attack?" repeatuntil="" returnvalue="evaluate"/>
						<action classname="elh.rain.actions.SendMessageAction" debugbreak="False" name="hit" namespace="" parameters="bWV0aG9kAHBhcmFt" parametervalues="SGl0AA==" repeatuntil=""/>
					</decision>
				</constraint>
				<constraint constraint="interactTarget != null" debugbreak="False" name="interact?" priority="" repeatuntil="">
					<decision classname="elh.rain.decisions.CloseEnoughConstraint" debugbreak="False" name="close enough?" namespace="" parameters="bW92ZVRhcmdldABjbG9zZUVub3VnaERpc3RhbmNl" parametervalues="bW92ZVRhcmdldAByYW5nZQ==" repeatuntil="">
						<action classname="elh.rain.actions.SendMessageAction" debugbreak="False" name="interact" namespace="" parameters="bWV0aG9kAHBhcmFt" parametervalues="SW50ZXJhY3QA" repeatuntil=""/>
					</decision>
				</constraint>
			</selector>
			<selector debugbreak="False" name="move" repeatuntil="" usepriorities="False">
				<constraint constraint="closeDistance &gt; 0" debugbreak="False" name="distance &gt; 0" priority="" repeatuntil="">
					<move closeenoughangle="" closeenoughdistance="closeDistance" debugbreak="False" facetarget="" movespeed="" movetarget="moveTarget" name="move until distance" repeatuntil="" turnspeed=""/>
				</constraint>
				<move closeenoughangle="" closeenoughdistance="" debugbreak="False" facetarget="" movespeed="" movetarget="moveTarget" name="move directly" priority="" repeatuntil="" turnspeed=""/>
			</selector>
		</parallel>
	</selector>
</behaviortree>